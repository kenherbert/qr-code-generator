//TODO: research and implement a free colour picker
//TODO: Add ability to save codes as images (or just print?)
var qrCode;

function makeDownloadable() {
    // Timeout required to yield until src attribute exists
    setTimeout(function() {
        var parent = document.getElementById('code-canvas');
        var img = parent.getElementsByTagName('img')[0];
        var link = document.createElement('a');

        parent.appendChild(link);
        link.setAttribute('href', img.getAttribute('src'));
        link.setAttribute('download', 'qr-code.png');
        link.appendChild(img);
    }, 1);
}


function drawCode() {
    qrCode.makeCode(document.getElementById('code-content').value);

    makeDownloadable();
}


function redrawCode() {
    document.getElementById('code-canvas').innerHTML = '';

    qrCode = new QRCode(document.getElementById('code-canvas'), {
        text: document.getElementById('code-content').value,
        width: document.getElementById('code-canvas').offsetWidth,
        height: document.getElementById('code-canvas').offsetWidth,
        colorDark : document.getElementById('colour-fg').value,
        colorLight : document.getElementById('colour-bg').value
    });

    makeDownloadable();
}


function resetCode() {
    document.getElementById('code-content').value = 'https://kenherbert.dev/';
    document.getElementById('colour-fg').value = '#000000';
    document.getElementById('colour-swatch-fg').style.backgroundColor = '#000000';
    document.getElementById('colour-bg').value = '#ffffff';
    document.getElementById('colour-swatch-bg').style.backgroundColor = '#ffffff';

    redrawCode();

    document.getElementById('code-content').focus();
}


var pickerForeground = new Picker({
    parent: document.getElementById('colour-swatch-fg'),
    alpha: false,
    color: document.getElementById('colour-fg').value,
    popup: 'bottom',
    onChange: function(colour) {
        document.getElementById('colour-fg').value = colour.hex();
        document.getElementById('colour-swatch-fg').style.backgroundColor = colour.hex();
        redrawCode();
    }
});


var pickerBackground = new Picker({
    parent: document.getElementById('colour-swatch-bg'),
    alpha: false,
    color: document.getElementById('colour-bg').value,
    popup: 'bottom',
    onChange: function(colour) {
        document.getElementById('colour-bg').value = colour.hex();
        document.getElementById('colour-swatch-bg').style.backgroundColor = colour.hex();
        redrawCode();
    }
});


document.getElementById('code-reset').onclick = resetCode;
document.getElementById('code-content').onkeyup = drawCode;
window.onresize = redrawCode;

redrawCode();