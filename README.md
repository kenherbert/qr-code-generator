# QR Code Generator

A simple QR Code maker with customisable background and foreground colours.

Uses the [QrCode.js](https://github.com/davidshimjs/qrcodejs) library.

Please note that this was made for a section of the HTML code to be copy/pasted into a flatpage in my [Django portfolio](https://gitlab.com/firedancersoftware/developer-portfolio) but is setup as a standalone page with no requirement for Python or Django for completeness.